
// This file has been generated by the GUI designer. Do not modify.

public partial class MainWindow
{
	private global::Gtk.Fixed fixed4;

	private global::Gtk.ComboBox cmbFrom;

	private global::Gtk.Entry txtCurrency;

	private global::Gtk.ComboBox cmbTo;

	private global::Gtk.Button button7;

	private global::Gtk.Label lblTotal;

	private global::Gtk.Label label6;

	private global::Gtk.Label label7;

	private global::Gtk.Label label8;

	protected virtual void Build()
	{
		global::Stetic.Gui.Initialize(this);
		// Widget MainWindow
		this.Name = "MainWindow";
		this.Title = global::Mono.Unix.Catalog.GetString("MainWindow");
		this.WindowPosition = ((global::Gtk.WindowPosition)(4));
		// Container child MainWindow.Gtk.Container+ContainerChild
		this.fixed4 = new global::Gtk.Fixed();
		this.fixed4.Name = "fixed4";
		this.fixed4.HasWindow = false;
		// Container child fixed4.Gtk.Fixed+FixedChild
		this.cmbFrom = global::Gtk.ComboBox.NewText();
		this.cmbFrom.AppendText(global::Mono.Unix.Catalog.GetString("IDR"));
		this.cmbFrom.AppendText(global::Mono.Unix.Catalog.GetString("USD"));
		this.cmbFrom.AppendText(global::Mono.Unix.Catalog.GetString("JPY"));
		this.cmbFrom.AppendText(global::Mono.Unix.Catalog.GetString("EUR"));
		this.cmbFrom.HeightRequest = 35;
		this.cmbFrom.Name = "cmbFrom";
		this.fixed4.Add(this.cmbFrom);
		global::Gtk.Fixed.FixedChild w1 = ((global::Gtk.Fixed.FixedChild)(this.fixed4[this.cmbFrom]));
		w1.X = 261;
		w1.Y = 53;
		// Container child fixed4.Gtk.Fixed+FixedChild
		this.txtCurrency = new global::Gtk.Entry();
		this.txtCurrency.HeightRequest = 35;
		this.txtCurrency.CanFocus = true;
		this.txtCurrency.Name = "txtCurrency";
		this.txtCurrency.IsEditable = true;
		this.txtCurrency.InvisibleChar = '•';
		this.fixed4.Add(this.txtCurrency);
		global::Gtk.Fixed.FixedChild w2 = ((global::Gtk.Fixed.FixedChild)(this.fixed4[this.txtCurrency]));
		w2.X = 37;
		w2.Y = 53;
		// Container child fixed4.Gtk.Fixed+FixedChild
		this.cmbTo = global::Gtk.ComboBox.NewText();
		this.cmbTo.AppendText(global::Mono.Unix.Catalog.GetString("IDR"));
		this.cmbTo.AppendText(global::Mono.Unix.Catalog.GetString("USD"));
		this.cmbTo.AppendText(global::Mono.Unix.Catalog.GetString("JPY"));
		this.cmbTo.AppendText(global::Mono.Unix.Catalog.GetString("EUR"));
		this.cmbTo.HeightRequest = 35;
		this.cmbTo.Name = "cmbTo";
		this.fixed4.Add(this.cmbTo);
		global::Gtk.Fixed.FixedChild w3 = ((global::Gtk.Fixed.FixedChild)(this.fixed4[this.cmbTo]));
		w3.X = 361;
		w3.Y = 53;
		// Container child fixed4.Gtk.Fixed+FixedChild
		this.button7 = new global::Gtk.Button();
		this.button7.WidthRequest = 391;
		this.button7.CanFocus = true;
		this.button7.Name = "button7";
		this.button7.UseUnderline = true;
		this.button7.Label = global::Mono.Unix.Catalog.GetString("Convert");
		this.fixed4.Add(this.button7);
		global::Gtk.Fixed.FixedChild w4 = ((global::Gtk.Fixed.FixedChild)(this.fixed4[this.button7]));
		w4.X = 37;
		w4.Y = 107;
		// Container child fixed4.Gtk.Fixed+FixedChild
		this.lblTotal = new global::Gtk.Label();
		this.lblTotal.WidthRequest = 391;
		this.lblTotal.Name = "lblTotal";
		this.lblTotal.LabelProp = global::Mono.Unix.Catalog.GetString("-");
		this.fixed4.Add(this.lblTotal);
		global::Gtk.Fixed.FixedChild w5 = ((global::Gtk.Fixed.FixedChild)(this.fixed4[this.lblTotal]));
		w5.X = 37;
		w5.Y = 169;
		// Container child fixed4.Gtk.Fixed+FixedChild
		this.label6 = new global::Gtk.Label();
		this.label6.Name = "label6";
		this.label6.LabelProp = global::Mono.Unix.Catalog.GetString("Nominal");
		this.fixed4.Add(this.label6);
		global::Gtk.Fixed.FixedChild w6 = ((global::Gtk.Fixed.FixedChild)(this.fixed4[this.label6]));
		w6.X = 40;
		w6.Y = 23;
		// Container child fixed4.Gtk.Fixed+FixedChild
		this.label7 = new global::Gtk.Label();
		this.label7.Name = "label7";
		this.label7.LabelProp = global::Mono.Unix.Catalog.GetString("From");
		this.fixed4.Add(this.label7);
		global::Gtk.Fixed.FixedChild w7 = ((global::Gtk.Fixed.FixedChild)(this.fixed4[this.label7]));
		w7.X = 261;
		w7.Y = 23;
		// Container child fixed4.Gtk.Fixed+FixedChild
		this.label8 = new global::Gtk.Label();
		this.label8.Name = "label8";
		this.label8.LabelProp = global::Mono.Unix.Catalog.GetString("To");
		this.fixed4.Add(this.label8);
		global::Gtk.Fixed.FixedChild w8 = ((global::Gtk.Fixed.FixedChild)(this.fixed4[this.label8]));
		w8.X = 361;
		w8.Y = 23;
		this.Add(this.fixed4);
		if ((this.Child != null))
		{
			this.Child.ShowAll();
		}
		this.DefaultWidth = 473;
		this.DefaultHeight = 224;
		this.Show();
		this.DeleteEvent += new global::Gtk.DeleteEventHandler(this.OnDeleteEvent);
		this.button7.Clicked += new global::System.EventHandler(this.Convert);
	}
}
