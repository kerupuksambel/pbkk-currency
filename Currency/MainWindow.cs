﻿using System;
using Gtk;
using System.Json;
using System.Net;

public partial class MainWindow : Gtk.Window
{
    private double GetRate(string fromCurrency, string toCurrency)
    {
        var res = "";
        double rate;
        try
        {
            string url = string.Format("https://free.currconv.com/api/v7/convert?q={0}_{1}&compact=ultra&apiKey=cd0304f3d4c6b7e923d3", fromCurrency.ToUpper(), toCurrency.ToUpper());
            string key = string.Format("{0}_{1}", fromCurrency.ToUpper(), toCurrency.ToUpper());

            res = new WebClient().DownloadString(url);
            JsonValue j = JsonArray.Parse(res);
            rate = j[key];
        }
        catch
        {
            rate = 0;
        }
        return rate;
    }

    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    protected void Convert(object sender, EventArgs e)
    {

        double Rate = GetRate(cmbFrom.ActiveText, cmbTo.ActiveText);
        double Cur = Double.Parse(txtCurrency.Text);
        lblTotal.Text = String.Concat(cmbTo.ActiveText, (Cur * Rate).ToString());
    }
}
